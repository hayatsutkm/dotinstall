# 02
=begin
数行のコメントが打てるよ
=end

=begin
print "Hello, world!"
puts "Hello, world!" # 改行
p "Hello, world!" # デバック用
=end

# 03
# 変数
# 英子文字、 _

=begin
msg = "Hello, world!"
puts msg

msg = "Hello, world again!"
puts msg
=end

# 定数
# 英大文字

=begin
VERSION = 1.1
puts VERSION

VERSION = 1.2
puts VERSION
=end

#04
=begin
str = "hello world"
float = 1.1
puts str.length
puts str.reverse
puts float.round
puts float.floor
=end

# 05
# 数値
# + - * / % **
=begin
p 10 + 3 #13
p 10 * 3 #30
p 2.4 * 3 #7.2
p 10 / 3 #3
p 10 % 3 #1
p 10.0 / 3 #3.33333
p Rational(2, 5) + Rational(3, 4) #23/20
p 2/5r + 3/4r #23/20
=end 
=begin
p 52.6.round # 四捨五入
p 52.6.floor # 小数点以下切り下げ
p 52.6.ceil # 小数点以下切り上げ
=end

# 06
# 文字列
# ""特殊文字、式展開
# ''
=begin
puts "hell\no worl\td"
puts 'hell\no worl\td'
=end

=begin
puts "price #{3000 * 4}"
puts 'price #{3000 * 4}'
=end

=begin
name = "takumi"
puts "hello #{name}"
=end

# + *
=begin
puts "hello " + "world"
puts "hello " * 10
=end

# 07
# !
# - upcase　大文字に変える
# - upcase!　元の文字列すら大文字に変える/破壊的なメソッド
# - downcase reverse
=begin
name = "takumi"

puts name.upcase
puts name
puts name.upcase!
puts name


# ? 真偽値　true false

p name.empty? # false
p name.include?("k") # ture
=end

# 08
# 配列
#colors = ["red","blue","yellow"]

=begin
p colors[0] # 添字
p colors[-1] # 末尾
p colors[0..2] # 0~2
p colors[0...2] # 0~2の直前　よって　0,1
p colors[5] # nil
=end
=begin
colors[0] = "pink"
colors[1..2] = ["white", "black"]
colors.push("gold")
colors << "silver"
p colors

p colors.size
p colors.sort
=end

# 配列
=begin
colors = ["red","blue","yellow"]

p colors[0] #添字
p colors[-1]
p colors[0..2]
p colors[0...2]
p colors[-5] #nil

p colors.size
p colors.sort
=end

#09
#ハッシュ
#　key / value

# taguchi 200
# koji 400
=begin
scores = {"taguchi" => 200, "koji" => 400}
scores = {:taguchi => 200, :koji => 400}
scores = {taguchi:200, koji: 400}

p scores[:taguchi]
scores[:koji] = 600
p scores

p scores.size
p scores.keys
p scores.values
p scores.has_key?(:taguchi)
=end

#10
#変換

=begin
x = 50
y = "3"

p x + y.to_i
p x + y.to_f
p x.to_s + y

scores = {taguchi: 200, koji: 400}
p scores.to_a.to_h
=end

# 11
# %
=begin
puts "hello"
puts 'hello'

puts %Q(hello) #= "hello"
puts %(hello) #= "hello"
puts %q(hello) #= 'hello'
#これらの記法はダブルクォーテーション、シングルクォーテーションを入れるときに便利

p ["red", "blue"]
p ['red', 'blue']

p %W(red blue) #=["red", "blue"]
p %w(red blue) #=['red', 'blue']

=end

#12
#"文字列" % 値
# %s
# %d
# %f
=begin
p "name: %s" % "taguchi"
p "name: %10s" % "taguchi"
p "name: %-10s" % "taguchi"

p "id: %05d, rate: %10.2f" % [355,3.284]

#printf
#sprintf

p sprintf("name: %10s\n", "taguchi")
p sprintf("id: %05d, rate: %10.2f", 355,3.284)

#13
#if
# > < >= <= == != 
# &&(AND) ||(OR) !(NOT)

printf "input your score:"
score = gets.to_i
=begin
if score > 80 then
  puts "great!!"
elsif score > 60 
  puts "good!"
else
  puts "so so..."
end

puts "great!!!" if score > 80
=end

#14
#case
=begin
signal = gets.chomp

case signal
when "red" then
  puts "stop!"
when "green","blue"
  puts "go!"
when "yellow" then
  puts "caution!"
else
  puts "wrong signal"
end
=end

#15
#while
=begin
i = 0
while i < 10 do
  puts "#{i}: hello"
  i += 1
end

#times

10.times do |i|
  puts "#{i}: hello"
end
=end

# 10.times { |i| puts "#{i}: hello" }

#16
# for
=begin
for i in 15..20 do
  p i
end

for color in ["red", "blue"] do
  p color
end

for name, score in {taguchi:200,koji:400} do
  puts "#{name}: #{score}"
end
=end
=begin
(15..20).each do |i|
  p i
end

["red", "blue"].each do |color|
  p color
end

{taguchi:200,koji:400}.each do |name, score|
  puts "#{name}: #{score}"
end
=end

#17
#loop
=begin
i = 0 
loop do
  p i
  i += 1
end
=end
#break
#next
=begin
10.times do|i|
  if i == 7 then
    #break
    next
  end
  p i
end
=end
#18
#メソッド
=begin
def sayHi(name = "tom") #引数
  score = 80
  #puts "hi!　#{name}"
  return "hi!　#{name}"
end

# sayHi("taku")
#sayHi "taku"
#sayHi 

p sayHi
p score
=end

#19,20,21
#クラス
#クラスメソッド、クラス変数
#クラスの軽傷
#アクセス権
# public
# protected
# private: レシーバを指定できない
=begin
class User
  @@count = 0
  VERSION = 1.1
  attr_accessor :name
  # attr_reader :name
  # setter: name=(value)
  # getter: name
=begin
  def initialize(name)
    @@count += 1
    @name = name
  end

  def sayHi
    puts "Hi!"
    #self
    #self.name => @name
    #puts "hello! I'm #{@name}"
    #puts "hello! I'm #{self.name}"
    #puts "hello! I'm #{name}"
    sayPrivare
    # self.sayPrivate
  end

  private 
    def sayPrivare
      puts "private" #User.new.sayPrivate #NG
    end
   
  def self.info
    puts "#{VERSION}: User class, #{@@count} instances."
  end
end


# User:親クラス、Super Class
# AdminUser:子クラス、Sub Class 
class AdminUser < User
  def sayHello
    puts "Hello"
    sayPrivare    
  end
  #オーバーライド
  def sayHi
    puts "Hi from admin!!"
  end
end
=begin 

tom = User.new("tom")
bob = User.new("bob")
steve = User.new("steve")


tom.name = "tom Jr."
p tom.name

tom.sayHi #レシーバ

bob.sayHi 

User.info
p User::VERSION

tom = AdminUser.new("tom")
tom.sayHi
tom.sayHello
=end
#User.new.sayHi
# AdminUser.new.sayHello

#24
#module
# ミックスイン
#-名前空間
=begin
module Debug
  def info #selfを付けないことで他のインスタンスとして使う「ミックスイン」
    puts "#{self.class} debug info..."
  end
end

class Player
  include Debug
end

class Monster
  include Debug
end

Player.new.info
Monster.new.info


=begin
def movie_encode
end
def movie_export
end

module Movie
  VERSION = 1.1
  def self.encode
    puts "encoding..."
  end
  def self.export
    puts "exporting..."
  end
end

Movie.encode
Movie.export
p Movie::VERSION
=end

#26
#例外処理
=begin
class MyError < StandardError;
end

x = gets.to_i

begin
  if x == 3 
    raise MyError
  end
  p 100/x
rescue MyError
  puts "not 3!!"
rescue => ex
  p ex.message
  p ex.class
  puts "stopped"
ensure
  puts "--END--"
end
=end
puts "                   .Wn
                        .6wW;
                        JzwVb
                       .$zwXW.
                      .$=lwXfh
                    ..6?=lrXWW,
   `  `  `  `  `..JT<>>?=truZWWh..  `  `  `  `
             .v6<<<:;<??ltrzZyVpfWWk(..         `
          .JC<<<__(<>??=lrvzuZyyfppppWXS.
       `.J1?<<__(<????=ltrvzuuZyyVffppppkZ,  `
   `   .S??>;;<>???uv""4yrzQ7^?7WVVffppppWZ;
      .D=???>>????j\ ., ,kw\.gN. NffppppWWkd
      ,zl==??????=d,.HM'.8wL "" .HfpfppWCOXd`
      ,zwOlllllllltd&...SzzuWXXHVQQkpff0<j0J
      .fXXwrttttrqHHmwvzzuuuZZXV6OWMpp0>(wG^
   `   ,sXZXXuzzzdHv?1TYUUUY9C??ugHfW01+Ow^
         7xwUyyZXZZXHka&&&&&ggWHpppWXwOu=       `
           _7uZwUUUWyyVVVVffffWWUUVO&v^
                ?7C1zuzzuuzzv777!"
                
