answer = rand(10) + 1  #rand(10):0~9の数字をランダムに返す
count = 0

loop do  #ループ文
  print "Your guess? "
  guess = gets.to_i
  count += 1
  
  if answer == guess
    puts "Bingo! It took #{count} guesses!"
    if count < 4
      puts "You are a person of genius!"
    end
    break
  elsif answer > guess
    puts "Bigger!"
  else
    puts "Smaller!"
  end
end

puts "See you, Good night!"