# 例外

class MyError < StandardError; end #自分で例外処理を作る。StandardErrorを継承
class MyError0 < StandardError; end #自分で例外処理を作る。StandardErrorを継承

x = gets.to_i


begin #例外が発生しそうなところでbegin~end
  if x > 100
    raise MyError0
  end
  if x == 3
    raise MyError #例外を投げる。
  end
  p 100 / x
rescue MyError0
  puts "100以下の数字を入れろ"  
rescue MyError #例外が発生しそうなところにrescue。キャッチ
  puts "not 3!"
rescue => ex #例外が発生しそうなところにrescue。rescue => ex発生した例外をオブジェクトに入れる。キャッチ
  p ex.message #オブジェクトが持っているメッセージを出力。Ruby が用意しているZeroDivisionErrorに引っかかる
  p ex.class #オブジェクト名の出力」
  puts "stopped!"
ensure #例外が発生しようがしまいが、最後に絶対実行したい処理はensureに続けて書く 
  puts "-- END --"
end