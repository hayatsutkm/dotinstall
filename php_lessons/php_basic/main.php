<?php
// comment out
# comment out
/* 
comment out 
*/

# 変数
$name = "dotinstall";
echo 'hello ' . $name . PHP_EOL;

# 特殊文字
# '
echo "it's " . $name . PHP_EOL;
# "
echo "it's $name" . PHP_EOL;
echo "it's \"$name\"" . PHP_EOL;
# tab
echo "it's \t \"$name\"" . PHP_EOL;

# ナウドキュメント ヒアドキュメント
$text = <<<"EOT"
      (YYﾌ
　　　　目
　ヘ ／￣￣＼
　＼/ (･)(･) ヽ
　 ｜　 ゜　　|
　 ヽ ＼＿／ ノ
  `○ｴｴ>ー――<ｴｴ○
     ｜ｏ　ｏ｜
      >―――<＼
    (＿_)(＿_)､
EOT;
echo $text . PHP_EOL;

# 演算
echo 1 + "2" . PHP_EOL; // これマジか  
